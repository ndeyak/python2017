import math

a_1 = input("Enter the coefficient a: ")
b_1 = input("Enter the coefficient b: ")
c_1 = input("Enter the coefficient c: ")


a,b,c = float(a_1),float(b_1),float(c_1)


d = b**2-4*a*c # discriminant

if d < 0:
    print ("This equation has no real solution")
elif d == 0:
    x = (-b+math.sqrt(b**2-4*a*c))/2*a
    print ("This equation has one solutions: ", x)
else:
    x1 = (-b+math.sqrt(b**2-4*a*c))/(2*a)
    x2 = (-b-math.sqrt(b**2-4*a*c))/(2*a)
    print ("This equation has two solutions: ", x1, " and", x2)
